package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.UUID;

public class CustomerManagerTest {
    private DBService dbService;
    private Logger logger;
    private CustomerManager customerManager;

    private Customer customerBeforeCreateMethod;
    private Customer customerAfterCreateMethod;

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        customerBeforeCreateMethod = new Customer()
                .setId(null)
                .setFirstName("John")
                .setLastName("Wayner")
                .setLogin("john_wick@gmail.com")
                .setPass("Baba_Jaga")
                .setBalance(0);
        customerAfterCreateMethod = customerBeforeCreateMethod.clone();
        customerAfterCreateMethod.setId(UUID.randomUUID());

        Mockito.when(dbService.createCustomer(customerBeforeCreateMethod)).thenReturn(customerAfterCreateMethod);

        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
    }

    @Test
    public void testCreateNewCustomer() {
        // Вызываем метод, который хотим протестировать
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);

        // Проверяем результат выполенния метода
        Assert.assertEquals(customer.getId(), customerAfterCreateMethod.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test
    public void testCreateCustomerWithNullArgument() {
        try {
            customerManager.createCustomer(null);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Argument 'customerData' is null.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithEasyPassword() {
        try {
            customerBeforeCreateMethod.setPass("123qwe");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password is easy.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithTooLongLastName() {
        try {
            customerBeforeCreateMethod.setLastName("TakoytoTakoytovich");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("LastName is too long.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithTooLongFirstName() {
        try {
            customerBeforeCreateMethod.setFirstName("TakoytoTakoytovich");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("FirstName is too long.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithTooShortLastName() {
        try {
            customerBeforeCreateMethod.setLastName("Qw");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("LastName is too short.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithTooShortFirstName() {
        try {
            customerBeforeCreateMethod.setFirstName("E");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("FirstName is too short.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithFirstNameContainsSpaces() {
        try {
            customerBeforeCreateMethod.setFirstName("Jo hn");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("FirstName contains spaces.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithLastNameContainsSpaces() {
        try {
            customerBeforeCreateMethod.setLastName("Wi ck");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("LastName contains spaces.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithLowercaseFirstName() {
        try {
            customerBeforeCreateMethod.setFirstName("john");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("FirstName contains only lowercase letters.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithLowercaseLastName() {
        try {
            customerBeforeCreateMethod.setLastName("wayne");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("LastName contains only lowercase letters.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithIllegalCharactersFirstName() {
        try {
            customerBeforeCreateMethod.setFirstName("John%_3");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("FirstName contains illegal characters.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithIllegalCharactersLastName() {
        try {
            customerBeforeCreateMethod.setLastName("Wick_#");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("LastName contains illegal characters.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithTooLongPassword() {
        try {
            customerBeforeCreateMethod.setPass("1234567890123");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password is too long.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithInvalidLogin() {
        try {
            customerBeforeCreateMethod.setLogin("chto~to@gmail.com");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("e-mail contains illegal characters.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithNullPass() {
        try {
            customerBeforeCreateMethod.setPass(null);
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("pass is null", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithNullFirstName() {
        try {
            customerBeforeCreateMethod.setFirstName(null);
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("FirstName is null", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithNullLastName() {
        try {
            customerBeforeCreateMethod.setLastName(null);
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("LastName is null", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithNullLogin() {
        try {
            customerBeforeCreateMethod.setLogin(null);
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Login is null", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithIllegalPass() {
        try {
            customerBeforeCreateMethod.setPass("Wayner"); // equals lastName
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password contains part of login/FirstName/LastName", ex.getMessage());
        }
    }
    @Test
    public void testCreateCustomerWithWrongWritingEmail() {
        try {
            customerBeforeCreateMethod.setLogin("Wayner"); // equals lastName
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Проверьте корректность e-mail'а", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithNullBalance() {
        try {
            customerBeforeCreateMethod.setBalance(12); // equals lastName
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("balance is not zero", ex.getMessage());
        }
    }

}
