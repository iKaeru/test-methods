package ru.nsu.fit.endpoint.service.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.List;
import java.util.UUID;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomerManager extends ParentManager {
    public CustomerManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    Pattern p = Pattern.compile("^[A-Za-z]");

    /**
     * Метод создает новый объект типа Customer. Ограничения:
     * Аргумент 'customerData' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же email;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать части login, firstName, lastName
     * money - должно быть равно 0.
     */
    public Customer createCustomer(Customer customer) {
        Validate.isTrue(customer != null, "Argument 'customerData' is null.");
        Validate.notNull(customer.getPass(), "pass is null");
        Validate.notNull(customer.getFirstName(), "FirstName is null");
        Validate.notNull(customer.getLastName(), "LastName is null");
        Validate.notNull(customer.getLogin(), "Login is null");
        Validate.isTrue(customer.getBalance() == 0, "balance is not zero");

        Validate.isTrue(!(customer.getPass().length() >= 13), "Password is too long.");
        Validate.isTrue(!(customer.getPass().length() < 6), "Password is too short.");
        Validate.isTrue(!(customer.getFirstName().length() >= 13), "FirstName is too long.");
        Validate.isTrue(customer.getFirstName().length() > 1, "FirstName is too short.");
        Validate.isTrue(!(customer.getLastName().length() >= 13), "LastName is too long.");

        Validate.isTrue((customer.getLastName().length() > 1), "LastName is too short.");
        Validate.isTrue(!customer.getPass().equalsIgnoreCase("123qwe") && !customer.getPass().equalsIgnoreCase("1q2w3e"), "Password is easy.");
        Validate.isTrue(!customer.getPass().contains(customer.getLogin()) &&
                        !customer.getPass().contains(customer.getFirstName()) &&
                        !customer.getPass().contains(customer.getLastName()), "Password contains part of login/FirstName/LastName");
        Validate.isTrue(customer.getLogin().contains("@"), "Проверьте корректность e-mail'а");
        Validate.isTrue(!p.matcher(customer.getFirstName()).matches(), "FirstName contains illegal characters.");

        Validate.isTrue(!p.matcher(customer.getLastName()).matches(), "LastName contains illegal characters.");
        Validate.isTrue(!customer.getLogin().contains("#") &&
                        !customer.getLogin().contains("$") &&
                        !customer.getLogin().contains("~"), "e-mail contains illegal characters.");
        Validate.isTrue(!customer.getFirstName().contains(" "), "FirstName contains spaces.");
        Validate.isTrue(!customer.getLastName().contains(" "), "LastName contains spaces.");
        Validate.isTrue((customer.getFirstName().toCharArray()[0]<= 'Z') && (customer.getFirstName().toCharArray()[0] >= 'A'), "FirstName contains only lowercase letters.");

        Validate.isTrue((customer.getLastName().toCharArray()[0]<= 'Z') && (customer.getLastName().toCharArray()[0] >= 'A'), "LastName contains only lowercase letters.");
        return dbService.createCustomer(customer);
    }

    /**
     * Метод возвращает список объектов типа customer.
     */
    public List<Customer> getCustomers() {
        return dbService.getCustomers();
    }


    /**
     * Метод обновляет объект типа Customer.
     * Можно обновить только firstName и lastName.
     */
    public Customer updateCustomer(Customer customer) {
        throw new NotImplementedException("Please implement the method.");
    }

    public void removeCustomer(UUID id) {
        throw new NotImplementedException("Please implement the method.");
    }

    /**
     * Метод добавляет к текущему баласу amount.
     * amount - должен быть строго больше нуля.
     */
    public Customer topUpBalance(UUID customerId, int amount) {
        throw new NotImplementedException("Please implement the method.");
    }
}
